<?php 

class CurrencyExchange {

	protected $_appID = '26c102aa26c84e20acd4673d624bc9b8';
	protected $_url = 'https://openexchangerates.org/api/';

	// Fetch the exchange rates via the API
	public function getExchangeRates($currencies = null) {

		$endpoint = 'latest.json';
		$auth = '?app_id=' . $this->_appID;
		$getURL = $this->_url . $endpoint . $auth;
		// Determine Currencies to get exchange rates for
		if (isset($currencies) && !empty($currencies)) {
			// Ensure currencies are a string
			if (is_array($currencies)) {
				$currencies = implode(',', $currencies);
			}
			$getURL = $getURL . '&symbols=' . urlencode($currencies);
		}
		// Open Curl Session
		$ch = curl_init($getURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30000);
		curl_setopt($ch, CURLOPT_URL, $getURL);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Get Exchange Rates
		$results = curl_exec($ch);
		curl_close($ch);
		// JOSN encode for use by javascript
		$results = json_decode($results);
		return $results;

	}

	// Calculate an exchange rate from a given exchange rate eg calculate the USD exchange rate from the AUD exchange rate
	public function calculateExchangeRate($rate) {

		$newRate = 1 / $rate;
		return $newRate;

	}

	// Calculate the exchanged value for a single currency
	public function calculateExchange($value, $rate) {

		$newValue = $value * $rate;
		return $newValue;

	}

	// Calculates the exchanged value for an array of exchange rates and returns in array with currency code as key
	public function bulkCalculateExchange($value, $rates) {

		$values = array();
		foreach ($rates as $name => $rate) {
			$newValue = $this->calculateExchange($value, $rate);
			$values[$name] = $newValue;
		}
		return $values;

	}
	
}