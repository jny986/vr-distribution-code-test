<?php 

require_once 'CurrencyExchange.php';

$CurrencyExchange = new CurrencyExchange();

header('content-type: application/json');

//Basic security for file
if (isset($_GET['aud_conversion']) && !empty($_GET['aud_conversion'])) {

	$currencies = 'AUD,JPY,GBP,NZD';
	// Get Exchange Rates
	$exchangeRates = $CurrencyExchange->getExchangeRates($currencies);
	// Calculate USD exchange rate to AUD
	$USD = $CurrencyExchange->calculateExchangeRate($exchangeRates->rates->AUD);
	$exchangeRates->rates->USD = $USD;
	// Unset the AUD from object as not needed anymore
	unset($exchangeRates->rates->AUD);
	// Calculate the Values
	$AUDValue = $_GET['amount'];
	$exchangedValues = $CurrencyExchange->bulkCalculateExchange($AUDValue, $exchangeRates->rates);
	$exchangeRates->values = $exchangedValues;
	// Format the date and time of retreival for later display
	$dateTime = new DateTime(strtotime($exchangeRates->timestamp), new DateTimeZone('Australia/Adelaide'));
	$dateTime = $dateTime->format('d/m/y h:i:s A e');
	$exchangeRates->dateTime = $dateTime;
	// JSON encode for javascript handling
	$result = json_encode($exchangeRates);

} else {

	$result = json_encode(array('error' => 'Please ensure that $_GET variables are set'));

}

echo $result;