(function($) {
	$('#aud-exchange-submit').click(function(e) {
		e.preventDefault();
		$.get('ajax.php', $('#aud-exchange').serialize(), function(data) {
			console.log(data);
			var audInput = $('#aud-exchange-value');
			var audValue = audInput.val();
			$('#aud-exchange-display-value').html(audValue);
			$('#aud-exchange-display-value').formatCurrency();
			var dateTime = data.dateTime;
			$('.retreived-at').each(function() {
				$(this).html(dateTime);
			});
			var USD = data.rates.USD;
			var USDRate = $('#usd-rate')
			USDRate.html(USD);
			USDRate.formatCurrency({roundToDecimalPlace:10});
			USD = data.values.USD;
			var USDValue = $('#usd-value');
			USDValue.html(USD);
			USDValue.formatCurrency();
			JPY = data.rates.JPY;
			var JPYRate = $('#jpy-rate');
			JPYRate.html(JPY);
			JPYRate.formatCurrency({symbol:"¥", roundToDecimalPlace:10});
			JPY = data.values.JPY;
			var JPYRate = $('#jpy-value');
			JPYRate.html(JPY);
			JPYRate.formatCurrency({symbol:"¥"});
			GBP = data.rates.GBP;
			var GBPRate = $('#gbp-rate');
			GBPRate.html(GBP);
			GBPRate.formatCurrency({symbol:"£", roundToDecimalPlace:10});
			GBP = data.values.GBP;
			var GBPRate = $('#gbp-value');
			GBPRate.html(GBP);
			GBPRate.formatCurrency({symbol:"£"});
			var NZD = data.rates.NZD;
			var NZDRate = $('#nzd-rate')
			NZDRate.html(NZD);
			NZDRate.formatCurrency({roundToDecimalPlace:10});
			NZD = data.values.NZD;
			var NZDValue = $('#nzd-value');
			NZDValue.html(NZD);
			NZDValue.formatCurrency();
			$('#aud-exchange-output').show();
		})
	})
})(jQuery);