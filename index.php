<!DOCTYPE html>
<html lang="en">
	<head>
	<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>VR Distribution - Application Code</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<nav class="navbar">
		</nav>
		<div class="container">
			<h1 class="text-center">VR Distribution - Application Code</h1>
			<h2 class="text-center">Currency Converter Code</h2>
			<div class="text-center" id="main-content">
				<form id="aud-exchange" action="ajax.php" method="GET" class="text-center">
					<input type="hidden" name="aud_conversion" value="true">\
					<p class="text-center">Enter A Value in AUD to Convert</p>
					<input id="aud-exchange-value" type="text" name="amount" value="" required>
					<input id="aud-exchange-submit" type="submit">
				</form>
				<div id="aud-exchange-output" style="display: none;">
					<h3>Converting: <span id="aud-exchange-display-value"></span> AUD</h3>
					<table  id="aud-exchange-output-table">
						<thead>
							<tr>
								<th>Currency</th>
								<th>Exchange Rate</th>
								<th>Converted Value</th>
								<th>Retrieved At</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>USD</td>
								<td id="usd-rate"></td>
								<td id="usd-value"></td>
								<td class="retreived-at"></td>
							</tr>
							<tr>
								<td>JPY</td>
								<td id="jpy-rate"></td>
								<td id="jpy-value"></td>
								<td class="retreived-at"></td>
							</tr>
							<tr>
								<td>GBP</td>
								<td id="gbp-rate"></td>
								<td id="gbp-value"></td>
								<td class="retreived-at"></td>
							</tr>
							<tr>
								<td>NZD</td>
								<td id="nzd-rate"></td>
								<td id="nzd-value"></td>
								<td class="retreived-at"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script src="js/jquery.formatCurrency-1.4.0.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>