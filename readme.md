VR Distribution - Application Code
==================================

The code in this repository allows for the conversion of any entered AUD figure into JPY, GBP, USD and NZD which is displayed in a table that appears below the AUD value input box.

Code Layout
-----------

### index.php
This is the form file that allows for the web interface of the code.

### ajax.php
This the code that takes the inputed amount for conversion and interacts with the CurrencyExchange class to get the exchange rate and calculate the exchanged value

### CurrencyExchange.php
Contains the CurrencyExchange class which interacts with the openexchangerates.org API to get the exchange rates and calculates the exchanged values

### /CSS
Contains the locally hosted CSS files used for the web interface

### /JS
Contains the locally hosted JS files used by the web interface